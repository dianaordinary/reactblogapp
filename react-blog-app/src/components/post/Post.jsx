import "./post.css";

export default function Post({img}) {
    return (
        <div className="post">
            <img
                src="https://images.unsplash.com/photo-1550079806-dde774a4d0cb?auto=format&fit=crop&q=80&w=2017&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
                alt=""
            />

            <div className="postInfo">
                <div className="postCats">
                    <span className="postCat">
                       postCat 1
                    </span>
                    <span className="postCat">
                       postCat 2
                    </span>
                </div>
                <span className="postTitle">
                   postTitle
                </span>
                <hr/>
                <span className="postDate">1 hour ago</span>
            </div>
            <p className="postDesc">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda
                officia architecto deserunt deleniti? Labore ipsum aspernatur magnam
                fugiat, reprehenderit praesentium blanditiis quos cupiditate ratione
                atque, exercitationem quibusdam, reiciendis odio laboriosam?
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda officia architecto deserunt
                deleniti? Labore ipsum aspernatur magnam fugiat, reprehenderit praesentium blanditiis quos cupiditate
                ratione atque, exercitationem quibusdam, reiciendis odio laboriosam?
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda officia architecto deserunt
                deleniti? Labore ipsum aspernatur magnam fugiat, reprehenderit praesentium blanditiis quos cupiditate
                ratione atque, exercitationem quibusdam, reiciendis odio laboriosam?
            </p>
        </div>
    );
}
