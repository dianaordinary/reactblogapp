import "./sidebar.css";

export default function Sidebar() {
    return (
        <div className="sidebar">
            <div className="sidebarItem">
                <span className="sidebarTitle">ABOUT ME</span>
                <img
                    src="https://images.unsplash.com/photo-1556897982-56edf57f1c5b?auto=format&fit=crop&q=80&w=1964&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
                    alt=""
                />
                <p>
                    Laboris sunt aute cupidatat velit magna velit ullamco dolore mollit
                    amet ex esse.Sunt eu ut nostrud id quis proident.
                </p>
            </div>
            <div className="sidebarItem">
                <span className="sidebarTitle">CATEGORIES</span>
                <ul className="sidebarList">
                    <li className="sidebarListItem">
                        Articles
                    </li>
                    <li className="sidebarListItem">
                        Posts
                    </li>
                    <li className="sidebarListItem">
                        Front-end
                    </li>
                    <li className="sidebarListItem">
                        Back-end
                    </li>
                    <li className="sidebarListItem">
                        Angular
                    </li>
                    <li className="sidebarListItem">
                        React
                    </li>
                    <li className="sidebarListItem">
                        Games
                    </li>
                    <li className="sidebarListItem">
                       Others
                    </li>
                </ul>
            </div>
            <div className="sidebarItem">
                <span className="sidebarTitle">FOLLOW US</span>
                <div className="sidebarSocial">
                    <i className="sidebarIcon fab fa-facebook-square"></i>
                    <i className="sidebarIcon fab fa-instagram-square"></i>
                    <i className="sidebarIcon fab fa-pinterest-square"></i>
                    <i className="sidebarIcon fab fa-twitter-square"></i>
                </div>
            </div>

        </div>
    );
}
