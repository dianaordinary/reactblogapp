import Sidebar from "../../components/sidebar/Sidebar";
import SinglePost from "../../components/singlePost/SinglePost";
import "./singlepage.css";

export default function Singlepage() {
    return (
        <div className="singlepage">
            <SinglePost />
            <Sidebar />
        </div>
    );
}
