import Topbar from "./components/topbar/Topbar";
import Homepage from "./pages/homepage/Homepage";
import Singlepage from "./pages/singlepage/Singlepage";
import Writepage from "./pages/writepage/Writepage";
import Settingpage from "./pages/settingpage/Settingpage";
import Login from "./pages/login/Login";
import Register from "./pages/register/Register";
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";

function App() {
    const currentUser = true; //если залогинены
    return (
        <Router>
            <Topbar/>
            <Routes>
                <Route exact path="/" element={<Homepage />}  />
                <Route  path="/posts" element={<Homepage />}  />

                <Route  path="/post/:id" element={<Singlepage />}  />
                <Route  path="/newpost" element={currentUser ? <Writepage /> : <Login />}  />
                <Route  path="/settings" element={currentUser ? <Settingpage /> : <Login />}  />

                <Route  path="/register" element={ <Register />}  />
                <Route  path="/login" element={ <Login />}  />
                {/*когда будет норм. авторизация
                <Route  path="/register" element={currentUser? <Homepage /> : <Register />}  />
                <Route  path="/login" element={currentUser? <Homepage /> : <Login />}  />
                */}
            </Routes>
        </Router>

    );
}

export default App;
