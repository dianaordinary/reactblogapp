Design React blog (without API). Using functional React components and React Router Dom and css.

![img.png](react-blog-app/assets/screens/img.png)
![img_1.png](react-blog-app/assets/screens/img_1.png)
![img_2.png](react-blog-app/assets/screens/img_2.png)

![img_3.png](react-blog-app/assets/screens/img_3.png)
![img_4.png](react-blog-app/assets/screens/img_4.png)

![img_5.png](react-blog-app/assets/screens/img_5.png)
![img_6.png](react-blog-app/assets/screens/img_6.png)
![img_7.png](react-blog-app/assets/screens/img_7.png)

+ Adaptive layout(flex)for tablets
![img_p.png](react-blog-app/assets/screens/img_p.png)
![img_p1.png](react-blog-app/assets/screens/img_p1.png)
